/**
 * ProductServiceTest
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.service;

import java.util.HashSet;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.patricia.novaes.productimage.model.Image;
import com.patricia.novaes.productimage.model.Product;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/resources/applicationContext.xml")
public class ProductServiceTest {
	@Autowired
	private ProductService productService;

	private Product product;

	private Image image;

	@After
	public void cleanup() {
		if (product != null) {
			productService.deleteProduct(product);
		}
	}

	private void createNewProduct() {
		product = new Product();
		product.setParentProduct(null);
		product.setName("PRODUCTONE");
		product.setDescription("PRODUCTONE");
		product.setImageList(new HashSet<Image>());
		productService.createProduct(product);
	}

	private void createNewImage() {
		createNewProduct();
		image = new Image();
		image.setProduct(product);
		image.setLocation("TESTE");
		image.setType("PNG");
	}

	@Test
	public void createProductTest() {
		createNewProduct();
		Assert.assertTrue(product.getId() > 0);
	}

	@Test
	public void getProductTest() {
		createNewProduct();
		Product result = productService.getProductFromId(product.getId());
		Assert.assertTrue(result != null);
		Assert.assertEquals(result.getId(), product.getId());
	}

	@Test
	public void updateProductTest() {
		createNewProduct();

		product.setName("PRODUCTONE");
		productService.updateProduct(product);

		Product result = productService.getProductFromId(product.getId());
		Assert.assertEquals(result.getName(), "PRODUCTONE");
	}

	@Test
	public void getAllProductsExcludingRelationship() {
		createNewProduct();
		List<Product> products = productService
				.getAllProductsExcludingRelationship();
		Assert.assertTrue(!products.contains(image));
	}

	@Test
	public void getAllProductsIncludingRelationship() {
		createNewProduct();
		List<Product> products = productService
				.getAllProductsIncludingRelationship();
		Assert.assertNotNull(products);
	}

	@Test
	public void getProductExcludingRelationshipById() {
		createNewProduct();
		Product productDao = productService
				.getProductExcludingRelationshipById(product.getId());
		Assert.assertNotNull(productDao);
	}

	@Test
	public void getProductIncludingRelationshipById() {
		createNewProduct();
		Product productDao = productService
				.getProductIncludingRelationshipById(product.getId());
		Assert.assertNotNull(productDao);
	}

	 @Test
	 public void getChildProductList(){
	 createNewProduct();
	 List<Product> products =
	 productService.getChildProductList(product.getId());
	 Assert.assertTrue(products.isEmpty());
	 }

	 @Test
	 public void getImageList(){
	 createNewImage();
	 List<Image> imagesDao = productService.getImageList(product.getId());
	 Assert.assertTrue(imagesDao != null);
	 }

}
