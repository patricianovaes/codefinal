/**
 * ImageService
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.service;

import org.springframework.stereotype.Service;

import com.patricia.novaes.productimage.model.Image;

@Service
public interface ImageService {
	public Image createImage(Image image);
	public Image getImageFromId(long imageId);
	public Image updateImage(Image image);
	public void deleteImage(Image image);
}
