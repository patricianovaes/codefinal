/**
 * ProductService
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.patricia.novaes.productimage.model.Image;
import com.patricia.novaes.productimage.model.Product;

@Service
public interface ProductService {
	public Product createProduct(Product parentProduct);
	public Product getProductFromId(long productId);
	public Product updateProduct(Product product);
	public void deleteProduct(Product product);

	public List<Product> getAllProductsExcludingRelationship();
	public List<Product> getAllProductsIncludingRelationship();
	public Product getProductExcludingRelationshipById(long productId);
	public Product getProductIncludingRelationshipById(long productId);
	public List<Product> getChildProductList(long productId);
	public List<Image> getImageList(long productId);
}
