/**
 * ImageServiceImpl
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import com.patricia.novaes.productimage.dao.ImageDao;
import com.patricia.novaes.productimage.model.Image;
import com.patricia.novaes.productimage.service.ImageService;

@Transactional
public class ImageServiceImpl implements ImageService {
	@Autowired
	private ImageDao imageDao;

	public Image createImage(Image image) {
		imageDao.save(image);
		return image;
	}

	public Image getImageFromId(long imageId) {
		return imageDao.getImageById(imageId);
	}

	public Image updateImage(Image image) {
		imageDao.update(image);
		return image;
	}

	public void deleteImage(Image image) {
		imageDao.delete(image);
	}
}
