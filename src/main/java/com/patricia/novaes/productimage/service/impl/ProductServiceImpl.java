/**
 * ProductServiceImpl
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import com.patricia.novaes.productimage.dao.ProductDao;
import com.patricia.novaes.productimage.model.Image;
import com.patricia.novaes.productimage.model.Product;
import com.patricia.novaes.productimage.service.ProductService;

@Transactional
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDao productDao;

	public Product createProduct(Product product) {
		productDao.save(product);
		return product;
	}

	public Product getProductFromId(long productId) {
		return productDao.getProductById(productId);
	}

	public Product updateProduct(Product product) {
		productDao.update(product);
		return product;
	}

	public void deleteProduct(Product product) {
		productDao.delete(product);
	}

	public List<Product> getAllProductsExcludingRelationship() {
		return productDao.getAllProductsExcludingRelationship();
	}

	public List<Product> getAllProductsIncludingRelationship() {
		return productDao.getAllProductsIncludingRelationship();
	}

	public Product getProductExcludingRelationshipById(long productId) {
		return productDao.getProductExcludingRelationshipById(productId);
	}

	public Product getProductIncludingRelationshipById(long productId) {
		return productDao.getProductIncludingRelationshipById(productId);
	}

	public List<Product> getChildProductList(long productId) {
		return productDao.getChildProductList(productId);
	}

	public List<Image> getImageList(long productId) {
		return productDao.getImageList(productId);
	}
}
