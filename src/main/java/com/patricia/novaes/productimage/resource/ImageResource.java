/**
 * ImageResource
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.patricia.novaes.productimage.model.Image;

@Component
@Path("/image")
public interface ImageResource {
	@POST
	@Path("/{productId}/{type}/{location}")
	@Produces("application/json")
	public Image createImage(@PathParam("productId") int productId,
			@PathParam("type") String type,
			@PathParam("location") String location);

	@GET
	@Path("/{imageId}")
	@Produces("application/json")
	public Image getImageFromId(@PathParam("imageId") long imageId);

	@PUT
	@Path("/{imageId}/{productId}/{type}/{location}")
	@Produces("application/json")
	public Image updateImage(@PathParam("imageId") long imageId,
			@PathParam("productId") int productId,
			@PathParam("type") String type,
			@PathParam("location") String location);

	@DELETE
	@Path("/{imageId}")
	public Response deleteImage(@PathParam("imageId") long imageId);
}