/**
 * ImageResourceImpl
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.resource.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.patricia.novaes.productimage.model.Image;
import com.patricia.novaes.productimage.model.Product;
import com.patricia.novaes.productimage.resource.ImageResource;
import com.patricia.novaes.productimage.service.ImageService;
import com.patricia.novaes.productimage.service.ProductService;

public class ImageResourceImpl implements ImageResource {
	@Autowired
	private ImageService imageService;
	@Autowired
	private ProductService productService;

	public Image createImage(int productId, String type, String location) {
		Image image = new Image();
		Product product = productService.getProductFromId(productId);
		image.setProduct(product);
		image.setType(type);
		image.setLocation(location);
		return imageService.createImage(image);
	}

	public Image getImageFromId(long imageId) {
		return imageService.getImageFromId(imageId);
	}

	public Image updateImage(long imageId, int productId, String type, String location) {
		Image image = imageService.getImageFromId(imageId);
		Product product = productService.getProductFromId(productId);
		image.setProduct(product);
		image.setType(type);
		image.setLocation(location);
		return imageService.updateImage(image);
	}

	public Response deleteImage(long imageId) {
		Image image = imageService.getImageFromId(imageId);
		imageService.deleteImage(image);
		return Response.status(200).entity("").build();
	}
}
