/**
 * ProductDao
 * 
 * @author Patricia Novaes 
 */

package com.patricia.novaes.productimage.dao;

import java.util.List;

import com.patricia.novaes.productimage.model.Image;
import com.patricia.novaes.productimage.model.Product;

public interface ProductDao {
	/**
	 * Save product
	 * 
	 * @param product
	 */
	public void save(Product product);

	/**
	 * Update product
	 * 
	 * @param product
	 */
	public void update(Product product);

	/**
	 * Delete product
	 * 
	 * @param product
	 */
	public void delete(Product product);

	/**
	 * Get Product By Id
	 * 
	 * @return Product
	 */
	public Product getProductById(long productId);

	/**
	 * Get All Products Excluding Relationship
	 * 
	 * @return List<Product>
	 */
	public List<Product> getAllProductsExcludingRelationship();

	/**
	 * Get All Products Including Relationship
	 * 
	 * @return List<Product>
	 */
	public List<Product> getAllProductsIncludingRelationship();

	/**
	 * Get Product Excluding Relationship By Id
	 * 
	 * @param productId
	 * @return Product
	 */
	public Product getProductExcludingRelationshipById(long productId);

	/**
	 * Get Product Including Relationship By Id
	 * 
	 * @param productId
	 * @return Product
	 */
	public Product getProductIncludingRelationshipById(long productId);

	/**
	 * Get Child Product List
	 * 
	 * @param productId
	 * @return List<Product>
	 */
	public List<Product> getChildProductList(long productId);

	/**
	 * Get Image List
	 * 
	 * @param productId
	 * @return List<Image>
	 */
	public List<Image> getImageList(long productId);
}