/**
 * ProductDaoImpl
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.patricia.novaes.productimage.dao.ProductDao;
import com.patricia.novaes.productimage.model.Image;
import com.patricia.novaes.productimage.model.Product;

public class ProductDaoImpl implements ProductDao {
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Save product
	 * 
	 * @param product
	 */
	public void save(Product product) {
		sessionFactory.getCurrentSession().persist(product);
	}

	/**
	 * Save product
	 * 
	 * @param product
	 */
	public void update(Product product) {
		sessionFactory.getCurrentSession().update(product);
	}

	/**
	 * Save product
	 * 
	 * @param product
	 */
	public void delete(Product product) {
		sessionFactory.getCurrentSession().delete(product);
	}

	/**
	 * Get product by id
	 * 
	 * @param Product
	 */
	public Product getProductById(long productId) {
		return (Product) sessionFactory.getCurrentSession().get(Product.class,
				productId);
	}

	/**
	 * Get All Products Excluding Relationship
	 * 
	 * @return List<Product>
	 */
	@SuppressWarnings("unchecked")
	public List<Product> getAllProductsExcludingRelationship() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("select * from product")
				.addEntity(Product.class);
		List<Product> result = query.list();
		return result;
	}

	/**
	 * Get All Products Including Relationship
	 * 
	 * @return List<Product>
	 */
	@SuppressWarnings("unchecked")
	public List<Product> getAllProductsIncludingRelationship() {
		Session session = sessionFactory.getCurrentSession();
		List<Product> result = session.createCriteria(Product.class).list();
		return result;
	}

	/**
	 * Get Product Excluding Relationship
	 * 
	 * @return Product
	 */
	@SuppressWarnings("unchecked")
	public Product getProductExcludingRelationshipById(long productId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session
				.createSQLQuery(
						"select * from product p where p.id = :productId")
				.addEntity(Product.class).setParameter("productId", productId);
		List<Product> result = query.list();
		return result.size() > 0 ? result.get(0) : null;
	}

	/**
	 * Get Product Including Relationship
	 * 
	 * @return Product
	 */
	public Product getProductIncludingRelationshipById(long productId) {
		return getProductById(productId);
	}

	/**
	 * Get child product
	 * 
	 * @return List<Product>
	 */
	@SuppressWarnings("unchecked")
	public List<Product> getChildProductList(long productId) {
		Session session = sessionFactory.getCurrentSession();
		List<Product> result = session.createCriteria(Product.class)
				.add(Restrictions.eq("parentProduct.id", productId)).list();
		return result;
	}

	/**
	 * Get Image List
	 * 
	 * @return List<Image>
	 */
	@SuppressWarnings("unchecked")
	public List<Image> getImageList(long productId) {
		Session session = sessionFactory.getCurrentSession();
		List<Image> result = session.createCriteria(Image.class)
				.add(Restrictions.eq("product", productId)).list();
		return result;
	}
}
