/**
 * ImageDao
 * 
 * @author Patricia Novaes 
 */

package com.patricia.novaes.productimage.dao;

import com.patricia.novaes.productimage.model.Image;

public interface ImageDao {
	/**
	 * Save image
	 * 
	 * @param image
	 */
	public void save(Image image);

	/**
	 * Save image
	 * 
	 * @param image
	 */
	public void update(Image image);

	/**
	 * Save image
	 * 
	 * @param image
	 */
	public void delete(Image image);

	/**
	 * Get image by id
	 * 
	 * @param image
	 */
	public Image getImageById(long imageId);
}