/**
 * ImageDaoImpl
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.dao.impl;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.patricia.novaes.productimage.dao.ImageDao;
import com.patricia.novaes.productimage.model.Image;

public class ImageDaoImpl implements ImageDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Save image
	 * 
	 * @param image
	 */
	public void save(Image image) {
		sessionFactory.getCurrentSession().persist(image);
	}
	
	/**
	 * Update image
	 * 
	 * @param image
	 */
	public void update(Image image) {
		sessionFactory.getCurrentSession().update(image);
	}
	
	/**
	 * Delete image
	 * 
	 * @param image
	 */
	public void delete(Image image) {
		sessionFactory.getCurrentSession().delete(image);
	}
	
	/**
	 * Get image by id
	 * 
	 * @param image
	 */
	public Image getImageById(long id) {
		return (Image) sessionFactory.getCurrentSession().get(Image.class, id);
	}
}
