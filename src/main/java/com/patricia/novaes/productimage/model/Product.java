/**
 * Product
 *
 * @author Patricia Novaes 
 */
package com.patricia.novaes.productimage.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "product", catalog = "productimage", uniqueConstraints = { @UniqueConstraint(columnNames = "name") })
public class Product {
	private long id;
	private Product parentProduct;
	private String name;
	private String description;
	private Set<Image> imageList;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "parent_product_id")
	public Product getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(Product parentProduct) {
		this.parentProduct = parentProduct;
	}

	@Column(name = "name", length = 200, unique = true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 200)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
	public Set<Image> getImageList() {
		return imageList;

	}

	public void setImageList(Set<Image> imageList) {
		this.imageList = imageList;
	}

	@Override
	public boolean equals(Object o) {
		Product otherProduct = (Product) o;
		return this.id == otherProduct.getId();
	}
}
